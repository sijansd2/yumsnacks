package com.sd.yumsnacks.Fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.sd.yumsnacks.Entites.RecipeListItem;
import com.sd.yumsnacks.Model.MyFirebase;
import com.sd.yumsnacks.R;
import com.sd.yumsnacks.activities.SearchActivity;
import com.sd.yumsnacks.adapters.RecipeListAdapter;
import com.sd.yumsnacks.viewmodel.RecipeListViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class RecipeImageFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 2;
    List<RecipeListItem> data;
    RecyclerView recyclerView;
    RecipeListAdapter adapter;
    SwipyRefreshLayout refreshLayout;
    public static String lastSeenKey;

    //ProgressDialog progressDialog;

    private RecipeListViewModel mViewModel;

    public RecipeImageFragment() {
    }

    @SuppressWarnings("unused")
    public static RecipeImageFragment newInstance(int columnCount) {
        RecipeImageFragment fragment = new RecipeImageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }

        //progressDialog = new ProgressDialog(getActivity());
       // progressDialog.setTitle("Please wait...");

        initViewModel();
    }

    @Override
    public void onResume() {
        super.onResume();
        getAllRecipes();
    }

    private void getAllRecipes() {

        if(data.size() == 0 ){
            mViewModel.getAllRecipes();
            //progressDialog.show();
        }
    }

    private void initViewModel() {
        mViewModel = ViewModelProviders.of(this).get(RecipeListViewModel.class);
        mViewModel.recipeImageItemMutableLiveData.observe(this, new Observer<List<RecipeListItem>>() {
            @Override
            public void onChanged(@Nullable List<RecipeListItem> recipeImageItems) {
                if(recipeImageItems.size()> MyFirebase.loadItemNum){
                    recipeImageItems.remove(0);
                }
                data.addAll(recipeImageItems);
                //Collections.reverse(data);
                adapter.addData(data);
                adapter.notifyDataSetChanged();
                //progressDialog.hide();
                refreshLayout.setRefreshing(false);
            }
        });
    }


        @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe, container, false);

            Context context = view.getContext();

            refreshLayout =  view.findViewById(R.id.swipyrefreshlayout);
            recyclerView =  view.findViewById(R.id.list);
            recyclerView.setNestedScrollingEnabled(false);
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            data = new ArrayList<>();
            adapter = new RecipeListAdapter(data);

            recyclerView.setAdapter(adapter);

            refreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh(SwipyRefreshLayoutDirection direction) {
                    mViewModel.getAllRecipes();
                }
            });

            FloatingActionButton searchIcon = view.findViewById(R.id.searchIcon);
            searchIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), SearchActivity.class));
                }
            });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
