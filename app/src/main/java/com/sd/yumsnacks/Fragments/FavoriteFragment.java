package com.sd.yumsnacks.Fragments;

import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sd.yumsnacks.Entites.RecipeListItem;
import com.sd.yumsnacks.Entites.User;
import com.sd.yumsnacks.MyApplication;
import com.sd.yumsnacks.R;
import com.sd.yumsnacks.activities.LoginActivity;
import com.sd.yumsnacks.activities.NavActivity;
import com.sd.yumsnacks.adapters.FavoriteAdapter;
import com.sd.yumsnacks.viewmodel.FavoriteViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class FavoriteFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 2;
    List<RecipeListItem> favData;
    RecyclerView recyclerViewFav;
    FavoriteViewModel mViewModel;
    FavoriteAdapter favAdapter;
    TextView email;

    @BindView(R.id.profile_image)ImageView profile_image;
    @BindView(R.id.name)TextView name;
    @BindView(R.id.favRecipes)TextView favRecipes;
    @BindView(R.id.logout)TextView logout;
    @BindView(R.id.noResultLay)LinearLayout noResultLay;
    @BindView(R.id.userLay)LinearLayout userLay;
    @BindView(R.id.btnLogin)Button btnLogin;

    public FavoriteFragment() {

    }

    @SuppressWarnings("unused")
    public static FavoriteFragment newInstance(int columnCount) {
        FavoriteFragment fragment = new FavoriteFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }

        initViewModel();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setAccount(){

        String nm = MyApplication.prefsHelper.getName();
        if(!TextUtils.isEmpty(nm)){
            //Picasso.get().load(url).into(profile_image);
            name.setText(nm);
        }
        else {
            getUserInfo();
        }
    }

    private void getUserInfo() {
        mViewModel.getUserInfo();
        mViewModel.getAllFavRecipes();
    }

    private void initViewModel() {
        mViewModel = ViewModelProviders.of(this).get(FavoriteViewModel.class);

        mViewModel.recipeFavItemMutableLiveData.observe(this, new Observer<List<RecipeListItem>>() {
            @Override
            public void onChanged(@Nullable List<RecipeListItem> recipeImageItems) {

                favData = recipeImageItems;
                Collections.reverse(favData);

                if(favData.size() > 0){
                   hideNoResult();
                }
                else {
                    showNoResult();
                }

                favAdapter.addData(favData);
                favAdapter.notifyDataSetChanged();

                if(favData.size() > 0){
                    favRecipes.setVisibility(View.VISIBLE);
                }
                else {
                    favRecipes.setVisibility(View.GONE);
                }
            }
        });

        mViewModel.response.observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {

                if(user != null){
                    setAccount();
                }
                else {

                }

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);
        ButterKnife.bind(this, view);

        email = view.findViewById(R.id.email);
        email.setText(MyApplication.prefsHelper.getEmail());

            Context context = view.getContext();
            recyclerViewFav =  view.findViewById(R.id.fav_list);
            recyclerViewFav.setLayoutManager(new GridLayoutManager(context, mColumnCount));

            favData = new ArrayList<>();
            favAdapter = new FavoriteAdapter(favData);
            recyclerViewFav.setAdapter(favAdapter);

        String uid = MyApplication.prefsHelper.getUdid();
        if(!TextUtils.isEmpty(uid)){
            logout.setVisibility(View.VISIBLE);
            userLay.setVisibility(View.VISIBLE);
            getUserInfo();
        }
        else {
            logout.setVisibility(View.GONE);
            userLay.setVisibility(View.GONE);
        }

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @OnClick(R.id.email)
    void emailClicked(){
        //new MyFirebase(getActivity()).getAllRecipes();
    }

    @OnClick(R.id.logout)
    void logoutClicked(){
        showLogoutDialog();
    }

    public void showLogoutDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dlg_logout);

        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavActivity activity = ((NavActivity)getActivity());
                activity.logout();
                activity.finish();
                dialog.dismiss();
            }
        });

        Button cancel = dialog.findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }

    @OnClick(R.id.btnLogin)
    public void login(){
        getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
    }

    private void showNoResult(){
        noResultLay.setVisibility(View.VISIBLE);
        String uid = MyApplication.prefsHelper.getUdid();
        if(!TextUtils.isEmpty(uid)){
            btnLogin.setVisibility(View.GONE);
        }
    }

    private void hideNoResult(){
        noResultLay.setVisibility(View.GONE);
    }


}
