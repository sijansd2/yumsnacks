package com.sd.yumsnacks.Entites;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {

    public String email;
    public String name;
    public String imageUrl;

    public User() {
    }

    public User(String email, String name, String imageUrl) {

        this.email = email;
        this.name = name;
        this.imageUrl = imageUrl;
    }


}