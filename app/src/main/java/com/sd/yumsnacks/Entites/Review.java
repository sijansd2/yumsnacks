package com.sd.yumsnacks.Entites;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Review {

    public String udid;
    public String email;
    public String name;
    public String comment;
    public float rate;

    public Review(String email, String name, String comment, float rate) {
        this.email = email;
        this.name = name;
        this.comment = comment;
        this.rate = rate;
    }

    public Review() {
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getUdid() {
        return udid;
    }

    public void setUdid(String udid) {
        this.udid = udid;
    }


    @Override
    public String toString() {
        return "Review{" +
                "comment='" + comment + '\'' +
                ", rate=" + rate +
                '}';
    }
}