package com.sd.yumsnacks.Entites;

import java.io.Serializable;

public class Shared implements Serializable{

    String recipeName;
    String category;
    boolean shared;

    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }


    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Shared() {
    }

    public Shared(String recipeName, String category, boolean isShared) {

        this.recipeName = recipeName;
        this.category = category;
        this.shared = isShared;
    }
}
