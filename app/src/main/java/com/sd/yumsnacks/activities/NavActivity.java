package com.sd.yumsnacks.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.sd.yumsnacks.Fragments.FavoriteFragment;
import com.sd.yumsnacks.Fragments.RecipeImageFragment;
import com.sd.yumsnacks.Model.MyFirebase;
import com.sd.yumsnacks.MyApplication;
import com.sd.yumsnacks.R;

public class NavActivity extends AppCompatActivity {

    Fragment fragment1;
    //Fragment fragment2;
    Fragment fragment3;
    Fragment active;
    FrameLayout content;
    FragmentManager fragmentManager;
    //TextView mTitle;
    BottomNavigationView navigation;
    private FirebaseAuth auth;
    private static MyFirebase myFirebase;
    private static NavActivity activity;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav);

        activity = this;

//        Toolbar toolbarTop = findViewById(R.id.toolbar_top);
//        mTitle = toolbarTop.findViewById(R.id.toolbar_title);
//        mTitle.setText("All Recipes");

        //get firebase auth instance
        auth = FirebaseAuth.getInstance();

        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        content = findViewById(android.R.id.content);
        fragmentManager = getSupportFragmentManager();

        fragment1 = RecipeImageFragment.newInstance(1);
        //fragment2 = RecipeVideoFragment.newInstance(1);
        fragment3 = FavoriteFragment.newInstance(2);
        active = fragment1;

        fragmentManager.beginTransaction().add(R.id.fragment_container, fragment3, "3").hide(fragment3).commit();
        //fragmentManager.beginTransaction().add(R.id.fragment_container, fragment2, "2").hide(fragment2).commit();
        fragmentManager.beginTransaction().add(R.id.fragment_container, fragment1, "1").commit();

        navigation.setSelectedItemId(R.id.navigation_all);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        String name = MyApplication.prefsHelper.getName();
//        if(TextUtils.isEmpty(name)){
//            navigation.setSelectedItemId(R.id.navigation_all);
//        }
//        else if(!MyApplication.prefsHelper.getIsAccSet()) {
//            FavoriteFragment frag = (FavoriteFragment) fragmentManager.findFragmentByTag("3");
//            if(frag != null){
//                frag.setAccount();
//                MyApplication.prefsHelper.setIsAccSet(true);
//                navigation.setSelectedItemId(R.id.navigation_all);
//            }
//        }
    }

    public void logout() {
        auth.signOut();
        MyApplication.prefsHelper.clear();
        startActivity(new Intent(this, LoginActivity.class));
    }

    public static MyFirebase getFirebaseInstance() {

        if (myFirebase == null) {
            myFirebase = new MyFirebase(getInstance());
        }

        return myFirebase;
    }

    public static NavActivity getInstance() {
        return activity;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getFirebaseInstance().removeAllListeners();
        myFirebase = null;
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_all:
                    fragmentManager.beginTransaction().hide(active).show(fragment1).commit();
                    active = fragment1;
                    //mTitle.setText("All Recipes");
                    return true;
                case R.id.navigation_account:
                    fragmentManager.beginTransaction().hide(active).show(fragment3).commit();
                    active = fragment3;
                    //mTitle.setText("Account");
                    return true;
            }
            return false;
        }
    };

}
