package com.sd.yumsnacks.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sd.yumsnacks.Entites.RecipeListItem;
import com.sd.yumsnacks.R;
import com.sd.yumsnacks.adapters.RecipeListAdapter;
import com.sd.yumsnacks.viewmodel.SearchActivityViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends AppCompatActivity {

    @BindView(R.id.rvSearch)RecyclerView rvSearch;
    @BindView(R.id.etSearch)EditText etSearch;
    @BindView(R.id.imvNoResult)ImageView imvNoResult;

    List<RecipeListItem> data;
    RecipeListAdapter adapter;

    SearchActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ButterKnife.bind(this);

        Toolbar toolbarTop = findViewById(R.id.toolbar_top);
        TextView mTitle = toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("Search Recipes");

        initViewModel();

        rvSearch.setLayoutManager(new LinearLayoutManager(this));
        data = new ArrayList<>();
        adapter = new RecipeListAdapter(data);
        rvSearch.setAdapter(adapter);

        getSearchedRecipes();

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(TextUtils.isEmpty(s)){
                    rvSearch.setVisibility(View.GONE);
                    imvNoResult.setVisibility(View.VISIBLE);
                }
                else {
                    imvNoResult.setVisibility(View.GONE);
                    rvSearch.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    adapter.getFilter().filter(s);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void getSearchedRecipes() {
        viewModel.getSearchedRecipes();
    }

    private void initViewModel() {

        viewModel = ViewModelProviders.of(this).get(SearchActivityViewModel.class);
        viewModel.recipeImageItemMutableLiveData.observe(this, new Observer<List<RecipeListItem>>() {
            @Override
            public void onChanged(@Nullable List<RecipeListItem> recipeImageItems) {
                data = recipeImageItems;
                adapter.addData(data);
                adapter.notifyDataSetChanged();

            }
        });
    }


}
