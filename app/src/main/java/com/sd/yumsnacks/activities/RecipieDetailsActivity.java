package com.sd.yumsnacks.activities;

import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sd.yumsnacks.Entites.RecipeDetails;
import com.sd.yumsnacks.Entites.RecipeListItem;
import com.sd.yumsnacks.MyApplication;
import com.sd.yumsnacks.R;
import com.sd.yumsnacks.viewmodel.RecipeDetailsViewModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RecipieDetailsActivity extends AppCompatActivity {

    RecipeListItem item;
    @BindView(R.id.image)ImageView image;
    @BindView(R.id.imageVideo)ImageView imageVideo;
    @BindView(R.id.name)TextView name;
    @BindView(R.id.ingredients)TextView ingredients;
    @BindView(R.id.process)TextView process;
    @BindView(R.id.review)TextView review;
    @BindView(R.id.postedBy)TextView postedBy;
    @BindView(R.id.category)TextView category;
    @BindView(R.id.ratingBar)RatingBar ratingBar;
    @BindView(R.id.reviewLay)LinearLayout reviewLay;
    @BindView(R.id.videoPart)RelativeLayout videoPart;
    @BindView(R.id.btnFavorite)ImageView btnFavorite;

    RecipeDetailsViewModel mViewModel;
    boolean isFavorite = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipie_details);

        ButterKnife.bind(this);

        if(getIntent() != null){
            item = (RecipeListItem) getIntent().getSerializableExtra("item");
        }

        if(item.getImgUrl()!=null && item.getImgUrl().startsWith("http")){
            Picasso.get().load(item.getImgUrl()).into(image);
        }

        name.setText(item.getName());
        review.setText("REVIEWS("+item.getReview()+")");
        ratingBar.setRating(Float.parseFloat(item.getRating()));

        initViewModel();
        getRecipeDetails();
        getIsFavorite(item);
    }

    private void getRecipeDetails() {
        mViewModel.getRecipeDetails(item.getKey());
    }

    private void getIsFavorite(RecipeListItem item) {
        mViewModel.getIsFavorite(item);
    }

    private void initViewModel() {
        mViewModel = ViewModelProviders.of(this).get(RecipeDetailsViewModel.class);
        mViewModel.response.observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {


                if(s.equals(getString(R.string.fav_add_success_label)) || s.equals("added")){
                    btnFavorite.setImageResource(R.mipmap.ic_heart_red);
                    isFavorite = true;
                }
                else {
                    btnFavorite.setImageResource(R.mipmap.ic_heart);
                    isFavorite = false;
                }
            }
        });

        mViewModel.responseDetails.observe(this, new Observer<RecipeDetails>() {
            @Override
            public void onChanged(@Nullable RecipeDetails recipeDetails) {
                ingredients.setText(recipeDetails.getIngredients());
                process.setText(recipeDetails.getProcess());
                postedBy.setText(recipeDetails.getPostedBy());
            }
        });
    }

    @OnClick(R.id.reviewLay)
    void reviewClicked(){

//        Intent intent = new Intent(this, ReviewActivity.class);
//        intent.putExtra("item", item);
//        startActivity(intent);
    }

    @OnClick(R.id.btnFavorite)
    void btnFavoriteClicked(){

        String udid = MyApplication.prefsHelper.getUdid();
        if(TextUtils.isEmpty(udid)){
            showLoginDialog();
           return;
        }

        if(isFavorite){
            removeFavorite();
        }
        else {
            addToFavorite(item);
        }

    }

    private void removeFavorite() {
        mViewModel.addToFavorite("", item);
    }

    private void addToFavorite(RecipeListItem item) {
        mViewModel.addToFavorite("added",item);
    }

    public void showLoginDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dlg_login);

        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RecipieDetailsActivity.this, LoginActivity.class));
                dialog.dismiss();
            }
        });

        Button cancel = dialog.findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }
}
