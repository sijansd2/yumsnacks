package com.sd.yumsnacks.utils;

import android.app.Activity;
import android.content.Intent;

public class ImageUtils {

    public static final int PICK_IMAGE_REQUEST = 71;
    public static final int PICK_VIDEO_REQUEST = 72;

    public static void choosePhoto(Activity mActivity){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        mActivity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

    }
}
