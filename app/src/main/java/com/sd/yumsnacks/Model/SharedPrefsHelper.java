package com.sd.yumsnacks.Model;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;


public class SharedPrefsHelper {

    public static final String MY_PREFS = "PK_GROUP_PREFS";

    SharedPreferences mSharedPreferences;

    public SharedPrefsHelper(Context context) {
        mSharedPreferences = context.getSharedPreferences(MY_PREFS, MODE_PRIVATE);
    }

    public void clear() {
        mSharedPreferences.edit().clear().apply();
    }



    String email = "";
    public String getEmail() {
        return mSharedPreferences.getString("email", null);
    }

    public void setEmail(String aaa) {
        this.email = aaa;
        mSharedPreferences.edit().putString("email", aaa).apply();
    }



    String name = "";
    public String getName() {
        return mSharedPreferences.getString("name", null);
    }

    public void setName(String aaa) {
        this.name = aaa;
        mSharedPreferences.edit().putString("name", aaa).apply();
    }


    String imgUrl = "";
    public String getImageUrl() {
        return mSharedPreferences.getString("imgUrl", null);
    }

    public void setImageUrl(String aaa) {
        this.imgUrl = aaa;
        mSharedPreferences.edit().putString("imgUrl", aaa).apply();
    }



    String udid = "";
    public String getUdid() {
        return mSharedPreferences.getString("udid", null);
    }

    public void setUdid(String aaa) {
        this.udid = aaa;
        mSharedPreferences.edit().putString("udid", aaa).apply();
    }


    boolean isAccSet = false;
    public boolean getIsAccSet() {
        return mSharedPreferences.getBoolean("accSet", false);
    }

    public void setIsAccSet(boolean aaa) {
        this.isAccSet = aaa;
        mSharedPreferences.edit().putBoolean("accSet", aaa).apply();
    }


}
