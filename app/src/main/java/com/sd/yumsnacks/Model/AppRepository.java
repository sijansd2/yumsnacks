package com.sd.yumsnacks.Model;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import com.sd.yumsnacks.Entites.RecipeDetails;
import com.sd.yumsnacks.Entites.RecipeListItem;
import com.sd.yumsnacks.Entites.Review;
import com.sd.yumsnacks.Entites.User;
import com.sd.yumsnacks.activities.NavActivity;

import java.util.List;

public class AppRepository {

    private static AppRepository ourInstance ;
    private Context context;

    public static AppRepository getInstance(Context context) {

        if(ourInstance == null){
            ourInstance = new AppRepository(context);
        }
        return ourInstance;
    }

    private AppRepository(Context context) {
        this.context = context;
    }

    public void gerAllRecipes(MutableLiveData<List<RecipeListItem>> recipeImageItemMutableLiveData) {
        MyFirebase myFirebase = NavActivity.getFirebaseInstance();
        myFirebase.getAllRecipes(recipeImageItemMutableLiveData);
    }


    public void addUserAcc(User user, MutableLiveData<String> response) {

        NavActivity.getFirebaseInstance().writeNewUser(user, response);
    }

    public void addToFavorite(String udid, RecipeListItem item, MutableLiveData<String> response) {
        NavActivity.getFirebaseInstance().addToFavorite(udid, item, response);
    }

    public void getIsFavorite(RecipeListItem item, MutableLiveData<String> response) {
        NavActivity.getFirebaseInstance().getIsFavorite(item, response);
    }

    public void getAllFavRecipes(MutableLiveData<List<RecipeListItem>> recipeFavItemMutableLiveData) {
        MyFirebase myFirebase = NavActivity.getFirebaseInstance();
        myFirebase.getAllFavRecipes(recipeFavItemMutableLiveData);

    }

    public void getUserInfo(MutableLiveData<User> response) {
        NavActivity.getFirebaseInstance().getUserInfo(response);
    }

    public void getSearchedRecipes(MutableLiveData<List<RecipeListItem>> liveData) {
        NavActivity.getFirebaseInstance().getSearchedRecipes(liveData);
    }

    public void getRecipeDetails(String key, MutableLiveData<RecipeDetails> responseDetails) {
        NavActivity.getFirebaseInstance().getRecipeDetails(key, responseDetails);
    }
}
