package com.sd.yumsnacks.Model;

import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.sd.yumsnacks.Entites.RecipeDetails;
import com.sd.yumsnacks.Entites.RecipeListItem;
import com.sd.yumsnacks.Entites.User;
import com.sd.yumsnacks.Fragments.RecipeImageFragment;
import com.sd.yumsnacks.MyApplication;
import com.sd.yumsnacks.R;

import java.util.ArrayList;
import java.util.List;

public class MyFirebase {

    private DatabaseReference mDatabase;
    ValueEventListener userRecipeListener;
    ValueEventListener recipeImageListener;
    ValueEventListener recipeVideoListener;
    ValueEventListener reviewListener;
    ValueEventListener favoriteListener;
    Activity context;
    String rootPath = "flamelink/environments/production/content/yumSnacks/en-US";

    ProgressDialog progressDialog;
    public static final int loadItemNum = 30;

    public MyFirebase(Activity activity){

        mDatabase = FirebaseDatabase.getInstance().getReference();
        this.context = activity;

        progressDialog = new ProgressDialog(activity);
        progressDialog.setTitle("Please wait...");

    }


    //Add user to db
    public void writeNewUser(final User user, final MutableLiveData<String> response) {

        String udid = MyApplication.prefsHelper.getUdid();
        mDatabase.child("users").child(udid).setValue(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //Toast.makeText(context, "Successfully Saved!", Toast.LENGTH_SHORT).show();
                        MyApplication.prefsHelper.setName(user.name);
                        MyApplication.prefsHelper.setImageUrl(user.imageUrl);
                        response.postValue("Successfully Added!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //Toast.makeText(context, "Failed!", Toast.LENGTH_SHORT).show();
                    }
                });

    }


    public void addToFavorite(final String val, RecipeListItem item, final MutableLiveData<String> response) {
        final String udid = MyApplication.prefsHelper.getUdid();
        String path = rootPath;
        Task<Void> query;
        if(val.length()>0){
            query = mDatabase.child(path).child(item.getKey()).child("favorites").child(udid).setValue(val);
        }
        else {
            query = mDatabase.child(path).child(item.getKey()).child("favorites").child(udid).removeValue();
        }

        query.addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //Toast.makeText(context, "Successfully Shared!", Toast.LENGTH_SHORT).show();
                        if(val.length() > 0){
                            response.postValue(context.getResources().getString(R.string.fav_add_success_label));
                        }
                        else {
                            response.postValue("");
                        }

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    public void getUserInfo(final MutableLiveData<User> response) {
        String udid = MyApplication.prefsHelper.getUdid();
        Query query = mDatabase.child("users").child(udid);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot == null)return;

                User user = dataSnapshot.getValue(User.class);
                response.postValue(user);
                if(user != null){
                    MyApplication.prefsHelper.setName(user.name);
                    MyApplication.prefsHelper.setImageUrl(user.imageUrl);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    //Get All recipes
    public void getAllRecipes(final MutableLiveData<List<RecipeListItem>> liveData){

        progressDialog.show();
        final List<RecipeListItem> itemList = new ArrayList<>();

        String path = rootPath;
        Query query;
        if(TextUtils.isEmpty(RecipeImageFragment.lastSeenKey)) {
             query = mDatabase.child(path).orderByKey().limitToFirst(loadItemNum);
        }else{
            query = mDatabase.child(path).orderByKey().startAt(RecipeImageFragment.lastSeenKey).limitToFirst(loadItemNum+1);
        }

        recipeImageListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot == null)return;

                itemList.clear();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                    RecipeListItem item = postSnapshot.child("recipeList").getValue(RecipeListItem.class);
                    item.setKey(postSnapshot.getKey());

                    RecipeImageFragment.lastSeenKey = postSnapshot.getKey();
                    itemList.add(item);
                }

                liveData.postValue(itemList);
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("aaaGetError", databaseError.getMessage());
                progressDialog.dismiss();
            }
        };
        query.addValueEventListener(recipeImageListener);

    }

    public void getRecipeDetails(String key, final MutableLiveData<RecipeDetails> responseDetails) {
        progressDialog.show();

        String path = rootPath+"/"+key+"/recipeDetails";
        Query query = mDatabase.child(path);

        recipeImageListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot == null)return;
                RecipeDetails item = dataSnapshot.getValue(RecipeDetails.class);
                progressDialog.dismiss();
                responseDetails.postValue(item);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("aaaGetError", databaseError.getMessage());
                progressDialog.dismiss();
            }
        };
        query.addValueEventListener(recipeImageListener);
    }

    public void getSearchedRecipes(final MutableLiveData<List<RecipeListItem>> liveData) {

        progressDialog.show();
        final List<RecipeListItem> itemList = new ArrayList<>();
        Query query;
        String path = rootPath;
        //query = mDatabase.child(path).orderByChild("recipeList/name").startAt(txt).endAt(txt+"\uf8ff");
        query = mDatabase.child(path).orderByChild("recipeList/name");



        recipeImageListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot == null)return;

                itemList.clear();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                    RecipeListItem item = postSnapshot.child("recipeList").getValue(RecipeListItem.class);
                    item.setKey(postSnapshot.getKey());

                    //if(item.getName().contains(txt)){
                        itemList.add(item);
                    //}
                }

                liveData.postValue(itemList);
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("aaaGetError", databaseError.getMessage());
                progressDialog.dismiss();
            }
        };
        query.addValueEventListener(recipeImageListener);
    }

    public void getAllFavRecipes(final MutableLiveData<List<RecipeListItem>> liveData) {

        String udid = MyApplication.prefsHelper.getUdid();
        final List<RecipeListItem> itemList = new ArrayList<>();

        String path = rootPath;
        Query query = mDatabase.child(path).orderByChild("favorites/"+udid).equalTo("added");

        favoriteListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot == null)return;

                itemList.clear();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {


                    RecipeListItem item = postSnapshot.child("recipeList").getValue(RecipeListItem.class);
                    item.setKey(postSnapshot.getKey());
                    itemList.add(item);

                }

                liveData.postValue(itemList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("aaaGetError", databaseError.getMessage());
            }
        };
        query.addValueEventListener(favoriteListener);
    }


    public void getIsFavorite(RecipeListItem item, final MutableLiveData<String> response) {

        String udid = MyApplication.prefsHelper.getUdid();
        if(udid == null) return;

        String path = rootPath;
        Query query = mDatabase.child(path).
                child(item.getKey()).child("favorites").child(udid);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.getValue() == null){
                    response.postValue("notAdded");
                }
                else if(dataSnapshot.getValue().equals("added")){
                    response.postValue("added");
                }
                else {
                    response.postValue("notAdded");
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("aaaGetError", databaseError.getMessage());
            }
        });

    }




    public void removeAllListeners(){

        if (userRecipeListener != null)
            mDatabase.removeEventListener(userRecipeListener);

        if (recipeImageListener != null)
            mDatabase.removeEventListener(recipeImageListener);

        if (recipeVideoListener != null)
            mDatabase.removeEventListener(recipeVideoListener);

        if (reviewListener != null)
            mDatabase.removeEventListener(reviewListener);

        if (favoriteListener != null)
            mDatabase.removeEventListener(favoriteListener);
    }

}
