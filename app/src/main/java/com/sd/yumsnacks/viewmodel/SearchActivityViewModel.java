package com.sd.yumsnacks.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.sd.yumsnacks.Entites.RecipeListItem;
import com.sd.yumsnacks.Model.AppRepository;

import java.util.List;

public class SearchActivityViewModel extends AndroidViewModel {

    private AppRepository mRepository;
    public MutableLiveData<List<RecipeListItem>> recipeImageItemMutableLiveData = new MutableLiveData<>();

    public SearchActivityViewModel(@NonNull Application application) {
        super(application);

        mRepository = AppRepository.getInstance(application.getApplicationContext());
    }

    public void getSearchedRecipes() {
        mRepository.getSearchedRecipes(recipeImageItemMutableLiveData);
    }
}
