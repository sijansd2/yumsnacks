package com.sd.yumsnacks.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.sd.yumsnacks.Entites.RecipeDetails;
import com.sd.yumsnacks.Entites.RecipeListItem;
import com.sd.yumsnacks.Model.AppRepository;

public class RecipeDetailsViewModel extends AndroidViewModel {

    private AppRepository mRepository;
    public MutableLiveData<String> response = new MutableLiveData<>();
    public MutableLiveData<RecipeDetails> responseDetails = new MutableLiveData<>();

    public RecipeDetailsViewModel(@NonNull Application application) {
        super(application);

        mRepository = AppRepository.getInstance(application.getApplicationContext());
    }


    public void addToFavorite(String udid, RecipeListItem item) {
        mRepository.addToFavorite(udid, item, response);
    }

    public void getIsFavorite(RecipeListItem item) {
        mRepository.getIsFavorite(item, response);
    }

    public void getRecipeDetails(String key) {
        mRepository.getRecipeDetails(key, responseDetails);
    }
}
