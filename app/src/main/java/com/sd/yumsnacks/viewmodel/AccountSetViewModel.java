package com.sd.yumsnacks.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.sd.yumsnacks.Entites.User;
import com.sd.yumsnacks.Model.AppRepository;

public class AccountSetViewModel extends AndroidViewModel {

    private AppRepository mRepository;
    public MutableLiveData<String> response = new MutableLiveData<>();

    public AccountSetViewModel(@NonNull Application application) {
        super(application);

        mRepository = AppRepository.getInstance(application.getApplicationContext());
    }


    public void addUserAcc(User user) {
        mRepository.addUserAcc(user, response);
    }
}
