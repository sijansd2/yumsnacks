package com.sd.yumsnacks.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.sd.yumsnacks.Entites.RecipeListItem;
import com.sd.yumsnacks.Entites.User;
import com.sd.yumsnacks.Model.AppRepository;

import java.util.List;

public class FavoriteViewModel extends AndroidViewModel {

    private AppRepository mRepository;
    public MutableLiveData<User> response = new MutableLiveData<>();
    public MutableLiveData<List<RecipeListItem>> recipeFavItemMutableLiveData = new MutableLiveData<>();

    public FavoriteViewModel(@NonNull Application application) {
        super(application);

        mRepository = AppRepository.getInstance(application.getApplicationContext());
    }

    public void getAllFavRecipes() {
        mRepository.getAllFavRecipes(recipeFavItemMutableLiveData);
    }

    public void getUserInfo() {
        mRepository.getUserInfo(response);
    }
}
