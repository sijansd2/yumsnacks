package com.sd.yumsnacks.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.sd.yumsnacks.Entites.Review;
import com.sd.yumsnacks.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MyViewHolder>{

    private List<Review> data;

    public ReviewAdapter(List<Review> data){

        this.data=data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_review, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final Review item = data.get(position);

        holder.ratingBar.setRating(item.getRate());
        holder.comment.setText(item.getComment());
        holder.name.setText(item.getName());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addData(List<Review> data){
        this.data = data;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.ratingBar)RatingBar ratingBar;
        @BindView(R.id.comment)TextView comment;
        @BindView(R.id.name)TextView name;
        //@BindView(R.id.email)TextView email;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
