package com.sd.yumsnacks.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.sd.yumsnacks.Entites.RecipeListItem;
import com.sd.yumsnacks.R;
import com.sd.yumsnacks.activities.RecipieDetailsActivity;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.MyViewHolder> implements Filterable {

    private List<RecipeListItem> data;
    private List<RecipeListItem> tempData;

    public RecipeListAdapter(List<RecipeListItem> data){

        this.data = data;
        this.tempData = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recipe_image, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final RecipeListItem item = data.get(position);

        if(item.getImgUrl().startsWith("http")){
            Picasso.get().load(item.getImgUrl()).into(holder.image);
        }

        holder.name.setText(item.getName());
        holder.review.setText("REVIEWS("+item.getReview()+")");
        holder.ratingBar.setRating(Float.parseFloat(item.getRating()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), RecipieDetailsActivity.class);
                intent.putExtra("item", (Serializable) item);
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addData(List<RecipeListItem> data){
        this.data = data;
        this.tempData = data;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.image)ImageView image;
        @BindView(R.id.name)TextView name;
        @BindView(R.id.review)TextView review;
        @BindView(R.id.ratingBar)RatingBar ratingBar;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                FilterResults filterResults = null;
                try {

                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    data = tempData;
                } else {
                    List<RecipeListItem> filteredList = new ArrayList<>();
                    for (RecipeListItem row : tempData) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    data = filteredList;
                }

                filterResults = new FilterResults();
                filterResults.values = data;
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                data = (ArrayList<RecipeListItem>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
